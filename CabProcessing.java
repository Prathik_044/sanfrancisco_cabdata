import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.io.FileWriter;

//Usage: Cmd line parameters must include a filename and a number.

public class CabProcessing {
	public static void main(String args[]) {
		if(args.length == 0) {
			System.out.println("Please enter file name and a number as command line argument");
			return;
		}

		try {
			String curr_line;
			BufferedReader br;
			File infile = new File(args[0]);
			br = new BufferedReader(new FileReader(infile));
			
			String filename="";
			int pos = args[0].lastIndexOf(".");
			if (pos > 0) {
    			filename = args[0].substring(0, pos);
			}
			
			File outfile = new File(filename+".csv");
			File legend = new File("Legend.txt");
			FileWriter writer = new FileWriter(outfile,true);
			FileWriter writer2 = new FileWriter(legend,true);

			while((curr_line = br.readLine()) != null) {
				String data[] = curr_line.split(" ");
				if(data.length > 1) {
					writer.write(args[1]+","+data[0]+","+data[1]+","+data[3]+"\n");
				}
			}
			writer.flush();
			writer.close();
			writer2.write(filename+"  "+args[1]+"\n");
			writer2.flush();
			writer2.close();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
}