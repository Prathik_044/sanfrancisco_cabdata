@echo off
setlocal EnableDelayedExpansion

SET /A a=2
for %%b in (*.txt) do (
 java CabProcessing "%%b" "!a!"
 SET /A a=!a!+1
)