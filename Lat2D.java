import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.io.FileWriter;

public class Lat2D {
	public static void main(String args[]) {
		double min_lat =	37.599;
		double min_longi = -122.516;
		double x1,y1,x2,y2,time;
		int xcord,ycord;
		double radius = 111120;

		x1 = Math.toRadians(min_lat);
		y1 = Math.toRadians(min_longi);

		BufferedReader br = null;
		File infile = new File(args[0]);

		String filename="";
			int pos = args[0].lastIndexOf(".");
			if (pos > 0) {
    			filename = args[0].substring(0, pos);
			}

		File outfile = new File("XY"+filename+".txt");

		try{

			String curr_line;
			br = new BufferedReader(new FileReader(infile));
			FileWriter writer = new FileWriter(outfile,true);

			while((curr_line = br.readLine()) != null) {

				String data[] = curr_line.split(",");
				if(data.length != 1) {
					x2 = Math.toRadians(Double.parseDouble(data[1]));
					y2 = Math.toRadians(Double.parseDouble(data[2]));

					double angle1 = Math.acos(Math.sin(x2) * Math.sin(x2)
                      + Math.cos(x2) * Math.cos(x2) * Math.cos(y2 - y1));
					angle1 = Math.toDegrees(angle1);
					xcord = (int)(radius * angle1);

					angle1 = Math.acos(Math.sin(x1) * Math.sin(x2)
                      + Math.cos(x1) * Math.cos(x2) * Math.cos(y2 - y2));
					angle1 = Math.toDegrees(angle1);
					ycord = (int)(radius * angle1);

					time = Double.parseDouble(data[3]) - 1211068800;

					writer.write((int)time+" "+"t"+data[0]+" "+Math.abs(xcord)+" "+Math.abs(ycord)+"\n");
				}
			}
			writer.flush();
			writer.close();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
}