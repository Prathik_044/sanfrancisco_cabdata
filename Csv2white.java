import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.io.FileWriter;

//Usage: Cmd line parameters must include a filename and a number.

public class Csv2white {
	public static void main(String args[]) {
		if(args.length == 0) {
			System.out.println("Please enter file name and a number as command line argument");
			return;
		}

		try {
			String curr_line;
			BufferedReader br;
			File infile = new File(args[0]);
			br = new BufferedReader(new FileReader(infile));
			
			File outfile = new File("Output.txt");
			
			FileWriter writer = new FileWriter(outfile,true);
			

			while((curr_line = br.readLine()) != null) {
				String data[] = curr_line.split(",");
				if(data.length > 1) {
					writer.write(data[0]+" "+data[1]+" "+data[2]+" "+data[3]+"\n");
				}
			}
			writer.flush();
			writer.close();
			
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
}