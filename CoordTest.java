public class CoordTest {
	public static void main(String args[]) {
		double lat =	37.599;
		double longi = -122.516;
		double x1,y1,x2,y2;
		int xcord,ycord;
		double radius = 111120;
		double sta_1_lat = 37.781569;
		double sta_1_long = -122.416449;
		double sta_2_lat = 37.615489;
		double sta_2_long = -122.391772;

		x1 = Math.toRadians(lat);
		y1 = Math.toRadians(longi);

		x2 = Math.toRadians(sta_1_lat);
		y2 = Math.toRadians(sta_1_long);

		double angle1 = Math.acos(Math.sin(x2) * Math.sin(x2)
        	             + Math.cos(x2) * Math.cos(x2) * Math.cos(y2 - y1));
		angle1 = Math.toDegrees(angle1);
		xcord = (int)(radius * angle1);

		angle1 = Math.acos(Math.sin(x1) * Math.sin(x2)
                    + Math.cos(x1) * Math.cos(x2) * Math.cos(y2 - y2));
		angle1 = Math.toDegrees(angle1);
		ycord = (int)(radius * angle1);

		System.out.println("Node 1 coords: "+ xcord + "," + ycord);


		x2 = Math.toRadians(sta_2_lat);
		y2 = Math.toRadians(sta_2_long);

		angle1 = Math.acos(Math.sin(x2) * Math.sin(x2)
        	             + Math.cos(x2) * Math.cos(x2) * Math.cos(y2 - y1));
		angle1 = Math.toDegrees(angle1);
		xcord = (int)(radius * angle1);

		angle1 = Math.acos(Math.sin(x1) * Math.sin(x2)
                    + Math.cos(x1) * Math.cos(x2) * Math.cos(y2 - y2));
		angle1 = Math.toDegrees(angle1);
		ycord = (int)(radius * angle1);

		System.out.println("Node 2 coords: "+ xcord + "," + ycord);
	}
}
