import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.io.FileWriter;

public class BoundNodes {
	public static void main(String args[]) {
		double min_lat = 37.599;
		double max_lat = 37.8114;
		double min_long = -122.516;
		double max_long = -122.358;

		BufferedReader br = null;
		File infile = new File(args[0]);

		File outfile = new File("Bound"+args[0]);

		try{

			String curr_line;
			br = new BufferedReader(new FileReader(infile));
			FileWriter writer = new FileWriter(outfile, true);

			while((curr_line = br.readLine()) != null) {

				String data[] = curr_line.split(",");
				if(data.length != 1) {
					double lat = Double.parseDouble(data[1]);
					double lon = Double.parseDouble(data[2]);

					if(lat < min_lat) {
						lat = min_lat;
					}

					if(lat > max_lat) {
						lat = max_lat;
					}

					if(lon < min_long) {
						lon = min_long;
					}

					if(lon > max_long) {
						lon = max_long;
					}

					writer.write(data[0]+","+lat+","+lon+","+data[3]+"\n");
					writer.flush();
				}
			}
			writer.close();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
}